# skysql

A SQL interface for skyhook that is basic, but with some customizations for use with:
* kinetic
* single-cell processing

This SQL variant will try to map standard SQL constructs, in reasonable ways, to implementations for this environment.